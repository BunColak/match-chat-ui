import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import io from 'socket.io-client';
import axios from 'axios';

Vue.config.productionTip = false;

const socket = io(process.env.VUE_APP_API_SERVER);
Vue.prototype.$socket = socket;

const http = axios.create({
  baseURL: process.env.VUE_APP_API_SERVER,
});

Vue.prototype.$http = http;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
