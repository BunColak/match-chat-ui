const state = {
  username: '',
  gender: 0,
  name: '',
  lastname: '',
  preferredGender: 0
};
const mutations = {
  setUserState(state, newUserDetails) {
    state.username = newUserDetails.username;
    state.gender = newUserDetails.gender;
    state.name = newUserDetails.name;
    state.lastname = newUserDetails.lastname;
    state.preferredGender = newUserDetails.preferredGender;
  }
};
const actions = {
  setUserState(context, newUserDetails) {
    context.commit('setUserState', newUserDetails);
  }
};

export default { namespaced: true, state, mutations, actions };
