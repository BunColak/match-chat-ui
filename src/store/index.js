import Vue from 'vue';
import Vuex from 'vuex';

import userModule from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    activeChat: undefined
  },
  mutations: {
    changeActiveChat(state, chatId) {
      state.activeChat = chatId;
    }
  },
  actions: {
    changeActiveChat(context, chatId) {
      context.commit('changeActiveChat', chatId);
    }
  },
  modules: {
    user: userModule
  }
});
