import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home-page',
      component: require('./views/Home.vue').default
    },
    {
      path: '/login',
      name: 'login-page',
      component: require('./views/Login.vue').default
    }
  ]
})
